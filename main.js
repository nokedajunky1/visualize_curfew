let mymap;
const GENERATE_POINTS_BUTTON = document.getElementById("generate_points");
const DRAW_BUTTON = document.getElementById("draw_button");

///////////////////
createMap();

DRAW_BUTTON.addEventListener("click", drawGeoCurfew);
GENERATE_POINTS_BUTTON.addEventListener("click", generateFieldsForPolygon);

document.getElementById("curfew_shape").addEventListener("change", function () {
  let shape = this.value;
  const SQUARE_FORM = document.getElementById("square_form");
  const CIRCLE_FORM = document.getElementById("circle_form");
  const POLYGON_FORM = document.getElementById("polygon_form");

  switch (shape) {
    case "square":
      SQUARE_FORM.style.display = "block";
      CIRCLE_FORM.style.display = "none";
      POLYGON_FORM.style.display = "none";
      break;
    case "circle":
      SQUARE_FORM.style.display = "none";
      CIRCLE_FORM.style.display = "block";
      POLYGON_FORM.style.display = "none";
      break;
    case "polygon":
      SQUARE_FORM.style.display = "none";
      CIRCLE_FORM.style.display = "none";
      POLYGON_FORM.style.display = "block";
      break;
  }
});

/////////////////

function createMap() {
  const ZOOM_LEVEL = 13;
  const CENTER_VIEW_COORDS = [52.193691, 20.930148];
  const TILE_URL = "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png";
  const ATTRIBUTION ='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors';
  
  mymap = L.map("mapid").setView(CENTER_VIEW_COORDS, ZOOM_LEVEL);
  const tiles = L.tileLayer(TILE_URL, {
    attribution: ATTRIBUTION,
  });
  tiles.addTo(mymap);
}

function drawGeoCurfew() {
  let shape = document.getElementById("curfew_shape").value;
  
  switch(shape){
    case "square":
      const squareRectangleCoords = getSquareRectangleCoords();
      square = L.rectangle(squareRectangleCoords, { color: getChoosedColor() }).addTo(mymap);      
      drawMarker(squareRectangleCoords[0]);
      break;
    case "polygon":
      const polygonCoords = getPolygonCoords();
      polygon = L.polygon(polygonCoords, { color : getChoosedColor() }).addTo(mymap);
      drawMarker(polygonCoords[0]);
      break;
    case "circle":
      cir = L.circle(getCircleCenter(), getCircleRadius(), {color : getChoosedColor() }).addTo(mymap);
      drawMarker(getCircleCenter());
      break;
    default:
      break;
  }  
  
  clearFields();
}

function drawMarker(coordsToMarkerBind) {
  let marker = L.marker(coordsToMarkerBind);
  marker.bindTooltip(getCurfewName());
  marker.fire("mouseover");
  marker.addTo(mymap);
}

function getPolygonCoords() {
  let coords = [];
  let inputs = document.getElementsByClassName("polygon");
  
  for (let i = 0; i < inputs.length; i++){
    coords.push([
      parseFloat(inputs[i].value.split(",")[0]), parseFloat(inputs[i].value.split(",")[1])
    ]);
  }
  
  return coords;
}

function getSquareRectangleCoords() {
  let coords = [];
  let rightTopCoordsPassedByUser = document.getElementById("right_top").value;
  let leftBottomCoordsPassedByUser = document.getElementById("left_bottom").value;

  coords.push([
    parseFloat(rightTopCoordsPassedByUser.split(",")[0]), parseFloat(rightTopCoordsPassedByUser.split(",")[1])
  ]);
  coords.push([
    parseFloat(leftBottomCoordsPassedByUser.split(",")[0]), parseFloat(leftBottomCoordsPassedByUser.split(",")[1])
  ]);

  return coords;
}

function getCircleCenter() {
  let center = [];
  let circleCenterCoordsPassedByUser = document.getElementById("circle_center")
    .value;
  //center coords are given in format 52.1844826, 20.93359 so i need to split them
  center.push(parseFloat(circleCenterCoordsPassedByUser.split(",")[0]));
  center.push(parseFloat(circleCenterCoordsPassedByUser.split(",")[1]));
  return center;
}

function getCircleRadius() {
  return parseFloat(document.getElementById("circle_radius").value);
}

function getChoosedColor() {
  let color;
  let combobox = document.getElementById("combobox");
  let choosedOption = combobox.options[combobox.selectedIndex].value;

  switch (choosedOption) {
    case "exclusive":
      color = "red";
      break;
    case "inclusive":
      color = "green";
      break;
    default:
      break;
  }
  return color;
}

function generateFieldsForPolygon() {
  let pointsSelect = document.getElementById("polygon_points_number");
  let numberOfPoints = pointsSelect.options[pointsSelect.selectedIndex].value;
  let form = document.getElementById("points_form");
  
  //delete oldly created fields before creating new ones
  let createdFields = [].slice.call(document.getElementsByClassName("vertex"));
  if (createdFields.length > 0) {
    for (let i = 1; i <= createdFields.length; i++) {
      let fieldToRemove = document.getElementById(`field_${i}`);
      form.removeChild(fieldToRemove);
    }
  }
  
  for (let i = 1; i  <= numberOfPoints; i++){
    let div = document.createElement("div");
    div.id = `field_${i}`;
    div.className = "vertex"

    let input = document.createElement("input");
    input.type = "text";
    input.placeholder = "52.1953659, 21.004406";
    input.id = `point_${i}`;
    input.className = "point polygon";

    let label = document.createElement("label");
    label.innerHTML = `Point ${i} coords: `;

    div.appendChild(label);
    div.appendChild(input);
    form.appendChild(div);
  }
}

function clearFields() {
  let inputsToClear = document.getElementsByClassName("point");
  
  for (const inputToClear of inputsToClear) {
    inputToClear.value = "";
  }
}

function getCurfewName() {
  let curfewName = document.getElementById("curfewName");
  return curfewName.value;
}